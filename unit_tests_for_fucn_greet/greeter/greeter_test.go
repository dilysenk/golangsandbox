package greeter

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestGreetMessage(t *testing.T) {

	type testCase struct {
		name    string
		hour    int
		message string
	}

	tests := []testCase{

		{name: "Mitya", hour: 6, message: "Good morning"},
		{name: "Mitya", hour: 11, message: "Good morning"},

		{name: "Mitya", hour: 12, message: "Hello"},
		{name: "Mitya", hour: 17, message: "Hello"},

		{name: "Mitya", hour: 18, message: "Good evening"}, // bug исправлена проверка
		{name: "Mitya", hour: 21, message: "Good evening"},

		{name: "Mitya", hour: 22, message: "Good night"},

		// проверки на работу метода с аргументом name + доп граничные значения
		{name: " Mitya ", hour: 0, message: "Good night"},
		{name: "мitya", hour: 5, message: "Good night"},
	}

	for _, tc := range tests {
		expected := tc.message + " " + strings.Title(strings.Trim(tc.name, " ")) + "!"

		testname := fmt.Sprintf("(%v) + (%d), result = %v\n", tc.name, tc.hour, expected)
		t.Run(testname, func(t *testing.T) {
			result := Greet(tc.name, tc.hour)
			t.Logf("Greet(%v) + (%d), result = %v\n", tc.name, tc.hour, result)

			// Assert
			assert.Equal(t, expected, result,
				fmt.Sprintf("Неверно. Ожидалось %v,"+
					" фактический %v", expected, result))
		})

	}
}
