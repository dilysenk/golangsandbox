package homework7

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

//найти среднее арифметическое и медианное из всех чисел слайса

func TestMedian(t *testing.T) {
	// Arrange
	type testCase struct {
		testSlice []int
		expected  int
	}
	tests := []testCase{

		{
			testSlice: []int{1, 8, 9, 10, 5},
			expected:  9,
		},
		{
			testSlice: []int{},
			expected:  0,
		},
		{
			testSlice: []int{1, 3},
			expected:  2,
		},
		{
			testSlice: []int{1, 9999999999999},
			expected:  5000000000000,
		},
		{
			testSlice: []int{0, 0, 0, 0},
			expected:  0,
		},
	}

	// Act
	for _, tc := range tests {
		testname := fmt.Sprintf("medianFunc(%v), result = %d\n", tc.testSlice, tc.expected)
		t.Run(testname, func(t *testing.T) {
			result := medianFunc(tc.testSlice)
			t.Logf("medianFunc(%v), result = %d\n", tc.testSlice, result)

			// Assert
			assert.Equal(t, tc.expected, result,
				fmt.Sprintf("Неверное число. Ожидалось %d,"+
					" фактический %d", tc.expected, result))
		})
	}
}

func TestAverage(t *testing.T) {
	// Arrange
	type testCase struct {
		testSlice []int
		expected  float32
	}
	tests := []testCase{

		{
			testSlice: []int{1, 8, 9, 10, 5},
			expected:  6.000000,
		},
		{
			testSlice: []int{1, 3},
			expected:  2.000000,
		},
		{
			testSlice: []int{1, 9999999999999},
			expected:  4999999913984.000000,
		},
		{
			testSlice: []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			expected:  0,
		},
		{
			testSlice: []int{},
			expected:  0,
		},
	}

	// Act
	for _, tc := range tests {
		testname := fmt.Sprintf("averageFunc(%v), expected = %f\n", tc.testSlice, tc.expected)

		t.Run(testname, func(t *testing.T) {

			result := averageFunc(tc.testSlice)

			t.Logf("averageFunc(%v), result = %f\n", tc.testSlice, result)
			// Assert
			assert.Equal(t, tc.expected, result,
				fmt.Sprintf("Ожидалось %f,"+
					" фактический %f", tc.expected, result))
		})

	}
}

func medianFunc(inputSlice []int) int {
	if len(inputSlice) == 0 {
		return 0
	} else if len(inputSlice)%2 == 1 {
		var indexMedian = len(inputSlice) / 2
		return inputSlice[indexMedian]
	} else {
		var median = inputSlice[len(inputSlice)/2-1] + inputSlice[len(inputSlice)/2]
		return median / 2
	}
}

func averageFunc(inputSlice []int) float32 {
	if len(inputSlice) == 0 {
		return 0
	}

	var sumSlice = 0
	for _, i := range inputSlice {
		sumSlice += i
	}
	if sumSlice == 0 {
		return 0
	}
	return float32(sumSlice / len(inputSlice))
}
