package homework7

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"sort"
	"testing"
)

//найти среднее арифметическое и медианное из всех чисел слайса

func TestDifference(t *testing.T) {
	//Arrange
	type testCase struct {
		testSlice []int
		expected  int
	}

	tests := []testCase{
		{
			testSlice: []int{1, 8, 9, 10, 5},
			expected:  9,
		},
		{
			testSlice: []int{},
			expected:  0,
		},
		{
			testSlice: []int{1, 3},
			expected:  2,
		},
		{
			testSlice: []int{1, 9999999999999},
			expected:  9999999999998,
		},
		{
			testSlice: []int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			expected:  0,
		},
	}
	for _, tc := range tests {
		testname := fmt.Sprintf("differenceFunc(%v), result = %d\n", tc.testSlice, tc.expected)
		t.Run(testname, func(t *testing.T) {
			result := differenceFunc(tc.testSlice)
			t.Logf("differenceFunc(%v), result = %d\n", tc.testSlice, result)

			// Assert
			assert.Equal(t, tc.expected, result,
				fmt.Sprintf("Неверное чи"+
					"сло. Ожидалось %d,"+
					" фактический %d", tc.expected, result))
		})
	}
}

func differenceFunc(sliceInput []int) int {
	if len(sliceInput) == 0 {
		return 0
	}
	sort.Ints(sliceInput)
	return sliceInput[len(sliceInput)-1] - sliceInput[0]
}
