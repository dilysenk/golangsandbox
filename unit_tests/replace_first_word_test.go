package homework7

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

//найти разницу между самым большим и самым маленьким числом в слайсе

func TestMagicString(t *testing.T) {

	type testCase struct {
		sentence string
		expected string
	}
	tests := []testCase{
		{
			sentence: "hello world. with pleasure",
			expected: "Hello world. With pleasure.",
		},
		{
			sentence: "Hello world. with pleasure",
			expected: "Hello world. With pleasure.",
		},
		{
			sentence: "hello world. with pleasure.",
			expected: "Hello world. With pleasure.",
		},
		{
			sentence: "Hello world. with pleasure.",
			expected: "Hello world. With pleasure.",
		},
		{
			sentence: "Hello world. With pleasure.",
			expected: "Hello world. With pleasure.",
		},
	}

	for _, tc := range tests {
		testname := fmt.Sprintf("magicString(%s), result = %s\n", tc.sentence, tc.expected)
		t.Run(testname, func(t *testing.T) {
			result := magicString(tc.sentence)
			t.Logf("magicString(%v), result = %v\n", tc.sentence, result)

			// Assert
			assert.Equal(t, tc.expected, result,
				fmt.Sprintf("Неверный результат. Ожидалось %v,"+
					" фактический %v", tc.expected, result))
		})
	}
}

func magicString(sentence string) string {
	var target string
	if sentence[len(sentence)-1] == '.' {
		sentence = sentence[:len(sentence)-1]
	}
	listSentence := strings.Split(sentence, ". ")
	for _, sentence := range listSentence {
		firstLetter := strings.ToUpper(string(sentence[0]))
		sentenceWithUpLetter := firstLetter + sentence[1:] + ". "
		target += sentenceWithUpLetter
	}
	return target[:len(target)-1]
}
